<?php

// ==============================================================================================
// Licensed under the GPLv2 license
// ==============================================================================================
// @author	 WEBO Software (http://www.webogroup.com/)
// @version	0.1
// @copyright  Copyright &copy; 2013 Openstat, All Rights Reserved
// ==============================================================================================
if (!class_exists('Plugin')) {
	die('Hacking attemp!');
}

class PluginOpenstatcounter extends Plugin {
	public function Activate() {
		require(dirname(__FILE__) . '/openstat.counter.api.php');
		$this->exportSQL(dirname(__FILE__) . '/sql.sql');
		$this->ExportSQLQuery("INSERT INTO prefix_openstatcounter VALUES (1, '" .
		openstat_counter_api_add($this->User_GetUserCurrent()->getMail(), 'WEBO@LiveStreet') .
		"') ON DUPLICATE KEY UPDATE id=1;");
		return true;
	}
	public function Init() {
		global $openstatcounter_code;
		$config = require(dirname(dirname(dirname(__FILE__))) . '/config/config.local.php');
		$code = ModuleDatabase::GetConnect()->selectRow("SELECT code FROM " . $config['db']['table']['prefix'] . "openstatcounter WHERE id=1");
		if (!empty($code['code']) && !($oUser = $this->User_GetUserCurrent()) || !$oUser->isAdministrator()) {
		require(dirname(__FILE__) . '/openstat.counter.api.php');
		$openstatcounter_code = openstat_counter_api_code($code['code']);
		}
	}
}
?>