<?php

// ==============================================================================================
// Licensed under the GPLv2 license
// ==============================================================================================
// @author     WEBO Software (http://www.webogroup.com/)
// @version    0.1
// @copyright  Copyright &copy; 2013 Openstat, All Rights Reserved
// ==============================================================================================
class PluginOpenstatcounter_ActionOpenstatcounter extends ActionPlugin {
	public function Init() {}
	public function EventShutdown() {}
	public function RegisterEvent() {
	    $this->AddEvent('admin', 'EventAdmin');
	}
        protected function EventAdmin() {
            $this->oUserCurrent=$this->User_GetUserCurrent();
            if (!$this->oUserCurrent or !$this->oUserCurrent->isAdministrator()) {
                return $this->EventNotFound();
            }
            $config = require(dirname(dirname(dirname(dirname(dirname(__FILE__))))) . '/config/config.local.php');
            if (!empty($_POST['code'])) {
        	$code = preg_replace("![^0-9]!is", "", $_POST['code']);
        	ModuleDatabase::GetConnect()->selectRow("UPDATE " . $config['db']['table']['prefix'] . "openstatcounter SET code='" . $code . "' WHERE id=1");	
            }
            $code = ModuleDatabase::GetConnect()->selectRow("SELECT code FROM " . $config['db']['table']['prefix'] . "openstatcounter WHERE id=1");
	    $this->Viewer_Assign('OpenstatcounterCode', $code['code']);
	}
}
?>