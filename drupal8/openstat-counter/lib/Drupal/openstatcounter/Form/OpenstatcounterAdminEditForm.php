<?php
// ==============================================================================================
// Licensed under the GNU GPLv2 (LICENSE.txt)
// ==============================================================================================
// @author     WEBO Software (http://www.webogroup.com/)
// @version    0.1
// @copyright  Copyright &copy; 2013 Openstat, All Rights Reserved
// ==============================================================================================
namespace Drupal\openstatcounter\Form;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
class OpenstatcounterAdminEditForm implements ContainerInjectionInterface, FormInterface {
    protected $connection;
    public static function create(ContainerInterface $container) {
	return new static($container->get('database'));
    }
    public function __construct(Connection $connection) {
	$this->connection = $connection;
    }
    public function getFormID() {
	return 'openstat_admin_edit';
    }
    public function buildForm(array $form, array &$form_state, Node $node = NULL) {
	global $user;
	if (@is_file(dirname(__FILE__) . '/languages/' . $user->language . '.php')) {
	    @include(dirname(__FILE__) . '/languages/' . $user->language . '.php');
	} else {
	    @include(dirname(__FILE__) . '/languages/en.php');
	}
	$config = $this->configFactory->get('openstatcounter.settings');
	$settings = $config->get();
	$form['general'] = array(
	    '#type' => 'textfield',
	    '#title' => PLG_OPENSTATCOUNTER,
	    '#default_value' => $settings['code'],
	    '#size' => 10,
	    '#maxlength' => 20,
	    '#required' => TRUE,
	    '#description' => PLG_OPENSTATCOUNTER_ABOUT1 . $settings['code'] . PLG_OPENSTATCOUNTER_ABOUT2
	);
	return parent::buildForm($form, $form_state);
    }
    public function validateForm(array &$form, array &$form_state) {
	parent::validateForm($form, $form_state);
	$form_state['values']['code'] = round($form_state['values']['code']);
    }
    public function submitForm(array &$form, array &$form_state) {
	$config = $this->configFactory->get('openstatcounter.settings');
	$config->set('code', $form_state['values']['code']);
	parent::submitForm($form, $form_state);
    }
}
?>