Openstat Counter module for Drupal 7.x
------------------------------------
Analytics to tell anything about your website

Installation
------------------------------------
1. Copy all files from this package to /sites/all/modules/openstatcounter/
   directory.
2. Activate the Openstat Counter module through the 'Administer - Site
   building - Modules'.

Support and bug reports
-----------------------
Please submit support requests and bug reports via
https://www.openstat.ru/