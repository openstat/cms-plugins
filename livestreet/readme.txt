Openstat Counter plugin for LiveStreet
------------------------------------
Analytics to tell anything about your website

Installation
------------------------------------
1. Extract current archive to plugins/openstatcounter/
2. Go to Plugin Management in administrative panel
3. Activate Openstat Counter

Support and bug reports
-----------------------
Please submit support requests and bug reports via
https://www.openstat.ru/