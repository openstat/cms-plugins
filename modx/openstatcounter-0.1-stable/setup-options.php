<?php
$output = '';
switch ($options[xPDOTransport::PACKAGE_ACTION]) {
	case xPDOTransport::ACTION_INSTALL:
		$output = '<h2>Openstat Counter Installer</h2><p>Openstat Counter created specially for your website is gathering website visits and statistics.</p><p>All statistics are available on Openstat website. You need login and password to access them. If you haven\'t use Openstat before, you will have link to access Openstat in your mailbox - please check mail.</p><p>If you want to use another Openstat counter - just insert its code into this field and press \'Save\'. Please note: current website counter will stop gather statistics.</p><br />';
		break;
	case xPDOTransport::ACTION_UPGRADE:
	case xPDOTransport::ACTION_UNINSTALL:
		break;
}
return $output;