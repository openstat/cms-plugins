<?php
return array(
	'admin' => 'Openstat Counter',
	'code' => 'Openstat Counter code',
	'about1' => "<p>Openstat Counter created specially for your website is gathering website visits and statistics.</p>
<p>All statistics <a href='https://www.openstat.ru/counter/",
	'about2' => "/report'>are available on Openstat website</a>. You need login and password to access them. If you haven't use Openstat before, you will have link to access Openstat in your mailbox - please check mail.</p><p>If you want to use another Openstat counter - please go to extension settings, insert counter's code into the field 'Counter Code' and press 'Save'. Please note: current website counter will stop gather statistics.</p>",
	'submit' => 'Submit'
);

?>