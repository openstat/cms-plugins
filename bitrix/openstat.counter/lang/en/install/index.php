<?php
$MESS['OPENSTATCOUNTER_SETTINGS']='Openstat Counter Settings';
$MESS['OPENSTATCOUNTER_MODULE_NAME'] = 'Openstat Counter';
$MESS['OPENSTATCOUNTER_MODULE_DESCRIPTION'] = 'Analytics to tell anything about your website';
define('OPENSTATCOUNTER_MENU_ITEM', "Openstat Counter");
$MESS['OPENSTATCOUNTER_CODE']="Counter Code";
$MESS['OPENSTATCOUNTER_ABOUT1']="<p>Openstat Counter created specially for your website is gathering website visits and statistics.</p><p>All statistics <a href='https://www.openstat.ru/counter/";
$MESS['OPENSTATCOUNTER_ABOUT2']="/report'>are available on Openstat website</a>. You need login and password to access them. If you haven't use Openstat before, you will have link to access Openstat in your mailbox - please check mail..</p><p>If you want to use another Openstat counter - please go to extension settings, insert counter's code into the field 'Counter Code' and press 'Save'. Please note: current website counter will stop gather statistics.</p>"
$MESS['OPENSTATCOUNTER_SAVE'] = 'Save';
$MESS['OPENSTATCOUNTER_RESET'] = 'Reset';
?>