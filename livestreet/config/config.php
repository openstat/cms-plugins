<?php

// ==============================================================================================
// Licensed under the GPLv2 license
// ==============================================================================================
// @author     WEBO Software (http://www.webogroup.com/)
// @version    0.1
// @copyright  Copyright &copy; 2013 Openstat, All Rights Reserved
// ==============================================================================================
Config::Set('router.page.openstatcounter', 'PluginOpenstatcounter_ActionOpenstatcounter');

return $config;
?>