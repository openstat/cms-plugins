<?php
// ==============================================================================================
// Licensed under the GPLv2 license
// ==============================================================================================
// @author     WEBO Software (http://www.webogroup.com/)
// @version    0.1
// @copyright  Copyright &copy; 2013 Openstat, All Rights Reserved
// ==============================================================================================

require(dirname(__FILE__) . '/openstat.counter.api.php');
register_shutdown_function('openstatcounter_code');
function openstatcounter_code () {
    global $OPENSTATCOUNTER_SETTINGS;
    if (!empty($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], 'admin.php') === false) {
	if (!empty($OPENSTATCOUNTER_SETTINGS['code'])) {
	    echo openstat_counter_api_code($OPENSTATCOUNTER_SETTINGS['code']);
	}
    }
}

/*------------- Ниже следующий массив задаётся из административного раздела модуля -------*/
$NEW = array(
/* ABOCMS:START */
'openstatcounter_code' => '0',
/* ABOCMS:END */
);
global $OPENSTATCOUNTER_SETTINGS, $db;
$OPENSTATCOUNTER_SETTINGS = $NEW;
if (empty($OPENSTATCOUNTER_SETTINGS['code'])) {
    $db->query("SELECT username FROM core_users WHERE active=1 ORDER BY id ASC LIMIT 1");
    $res = $db->next_record();
    $OPENSTATCOUNTER_SETTINGS['code'] = openstat_counter_api_add($db->Record['username'], '', 'WEBO@ABOCMS');
/* save settings inside the same file */
    $c = @file_get_contents(__FILE__);
    $c = str_replace("'openstatcounter_code' => '" . '0' . "',", "'openstatcounter_code' => '" . $OPENSTATCOUNTER_SETTINGS['code']  . "',", $c);
    @file_put_contents(__FILE__, $c);
}
?>