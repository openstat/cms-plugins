<?php 

defined('HOSTCMS') || exit('HostCMS: access denied.');
class Openstatcounter_Module extends Core_Module {
	public $version = '0.1';
    public $date = '2013-12-11';
    public function __construct() {
	parent::__construct(); 
	$this->menu = array(
	    array(
		'sorting' => 260,
		'block' => 1,
		'name' => Core::_('Openstatcounter.model_name'),
		'href' => "/admin/openstatcounter/index.php",
		'onclick' => "$.adminLoad({path: '/admin/openstatcounter/index.php'}); return false"
	    )
	);
    }
    
    public function install() {
		require(dirname(__FILE__) . '/openstat.counter.api.php');
		$user = Core_QueryBuilder::select()->from('users')->where('id', '=', $_SESSION['current_users_id'])->execute()->asAssoc()->result();
		$code = openstat_counter_api_add(empty($user[0]['email']) ? 'admin@' . $_SERVER['HTTP_HOST'] : $user[0]['email'], '', 'WEBO@Host.CMS');
/* insert counter ID as module description */
		Core_QueryBuilder::update('modules')->columns(array('description' => $code))->where('path', '=', 'openstatcounter')->where('deleted', '=', '0')->execute();
    }
}
/* register page callback */
ob_start('openstatcounter_code');
function openstatcounter_code ($content) {
    if (!empty($content) && !empty($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], '/admin/') === false && empty($_SESSION['is_superuser'])) {
		require(dirname(__FILE__) . '/openstat.counter.api.php');
		$code = Core_QueryBuilder::select()->from('modules')->where('path', '=', 'openstatcounter')->where('deleted', '=', '0')->execute()->asAssoc()->result();
		return preg_replace("!(</body>)!is", openstat_counter_api_code($code[0]['description']) . "$1", $content);
    } else {
		return $content;
    }
}
?>