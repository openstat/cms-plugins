Openstat Counter exntension for Typo3
------------------------------------
Analytics to tell anything about your website

Installation
------------------------------------
1. Extract current archive to typo3conf/ext/openstat_counter/
2. Rename ext_conf_template.LANG.txt to ext_conf_template.txt (LANG is your
   preferred language)
3. Go to Extension Manager in administrative panel
4. Activate Openstat Counter

Support and bug reports
-----------------------
Please submit support requests and bug reports via
https://www.openstat.ru/