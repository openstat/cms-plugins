<?php

// ==============================================================================================
// Licensed under the GPLv2 license
// ==============================================================================================
// @author     WEBO Software (http://www.webogroup.com/)
// @version    0.1
// @copyright  Copyright &copy; 2013 Openstat, All Rights Reserved
// ==============================================================================================
class PluginOpenstatcounter_HookOpenstatcounter extends Hook {
	public $oUserCurrent = null;
	public $aConfig = null;
	public function RegisterHook() {
	    $this->AddHook('template_body_end', 'Openstatcounter');
	}
	public function Openstatcounter () {
	    global $openstatcounter_code;
	    echo $openstatcounter_code;
	}
}
?>