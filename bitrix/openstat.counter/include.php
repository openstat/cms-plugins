<?php

// ==============================================================================================
// Licensed under the GPLv2 license
// ==============================================================================================
// @author     WEBO Software (http://www.webogroup.com/)
// @version    0.1
// @copyright  Copyright &copy; 2013 Openstat, All Rights Reserved
// ==============================================================================================

class COpenstatCounter {

	function OnBeforeProlog ($content = false) {
		global $APPLICATION, $USER;
		if (!$USER->IsAdmin()) {
			@include(dirname(__FILE__) . '/install/openstat.counter.api.php');
			$APPLICATION->AddHeadString(openstat_counter_api_code(COption::GetOptionString('openstat.counter', 'OPENSTATCOUNTER_CODE')), true);
		}
	}
}

?>