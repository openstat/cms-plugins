<?php
global $DOCUMENT_ROOT, $MESS;

IncludeModuleLangFile(__FILE__);

if (class_exists("openstat_counter")) return;

class openstat_counter extends CModule {
	var $MODULE_ID = "openstat.counter";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "Y";

	function openstat_counter() {
		$arModuleVersion = array();
		include(dirname(__FILE__) . "/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->PARTNER_NAME = "Openstat";
		$this->PARTNER_URI = "https://www.openstat.ru/";
		$this->MODULE_NAME = GetMessage("OPENSTATCOUNTER_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("OPENSTATCOUNTER_MODULE_DESCRIPTION");
	}

	function DoInstall() {
		global $USER;
		RegisterModule($this->MODULE_ID);
		RegisterModuleDependences('main', 'OnBeforeProlog', $this->MODULE_ID, 'COpenstatCounter', 'OnBeforeProlog', 9999);
/* get counter code */
		include(dirname(__FILE__) . '/openstat.counter.api.php');
		openstat_counter_api_add($USER->GetEmail(), '', 'WEBO@Bitrix ' . SM_VERSION);
	}

	function DoUninstall() {
		UnRegisterModuleDependences('main', 'OnBeforeProlog', $this->MODULE_ID, 'COpenstatCounter', 'OnBeforeProlog');
		UnRegisterModule($this->MODULE_ID);
	}
}
?>