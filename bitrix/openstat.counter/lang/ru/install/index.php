<?php
$MESS['OPENSTATCOUNTER_SETTINGS']='Настройки счетчика Openstat';
$MESS['OPENSTATCOUNTER_MODULE_NAME'] = 'Счетчик Openstat';
$MESS['OPENSTATCOUNTER_MODULE_DESCRIPTION'] = 'Аналитика, которая расскажет все о вашем сайте';
define('OPENSTATCOUNTER_MENU_ITEM', "Счетчик Openstat");
$MESS['OPENSTATCOUNTER_MENU_ITEM_ALT'] = "Аналитика для сайта - Openstat";
$MESS['OPENSTATCOUNTER_AM_ALT'] = "Аналитика для сайта - Openstat";
$MESS['OPENSTATCOUNTER_AM'] = "Счетчик Openstat";
$MESS['OPENSTATCOUNTER_CODE']="Код счетчика Openstat";
$MESS['OPENSTATCOUNTER_ABOUT1']="<p>Счётчик Openstat, созданный специально для вашего сайта, уже собирает статистику посещений.</p><p>Результаты измерений <a href='https://www.openstat.ru/counter/";
$MESS['OPENSTATCOUNTER_ABOUT2']="/report'>публикуются на сайте Openstat</a>. Для просмотра нужны логин и пароль. Если вы ещё не регистрировались в Openstat, то ссылка для входа содержится в письме — проверьте почту.</p><p>Если вы хотите использовать другой счётчик Openstat, перейдите в настройки расширения и его код в поле «Код счетчика» и нажмите «Сохранить». Обратите внимание, что в этом случае сбор статистики нынешним счётчиком прекратится.</p>";
$MESS['OPENSTATCOUNTER_SAVE'] = 'Сохранить';
$MESS['OPENSTATCOUNTER_RESET'] = 'Сбросить';
?>