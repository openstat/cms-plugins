# Create plugins tarballs
# $Id$

DIST=	abocms.tar.gz \
	bitrix.tar.gz \
	drupal6.tar.gz \
	drupal7.tar.gz \
	drupal8.tar.gz \
	hostcms6.tar.gz \
	joomla.tar.gz \
	livestreet.tar.gz \
	modx.tar.gz \
	typo3.tar.gz \
	wordpress.tar.gz


all: ${DIST}

clean: 
	rm *.tar.gz

${DIST}:
	tar czf $@ ${@:.tar.gz=}

