Openstat Counter module for ABO.CMS
------------------------------------
Analytics to tell anything about your website

Installation
------------------------------------
1. Extract this archive to mod/openstatcounter folder.
   Note: mod/openstatcounter/config.php must be writable for your web server.
2. Execute sql from mod/openstatcounter/sql .
3. Insert your Openstat Counter ID if you have one for this website.

Support and bug reports
-----------------------
Please submit support requests and bug reports via
https://www.openstat.ru/