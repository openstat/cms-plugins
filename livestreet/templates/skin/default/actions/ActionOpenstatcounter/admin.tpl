{include file='header.tpl'}

<div>
	<h2 class="page-header">{$aLang.plugin.openstatcounter.admin}</h2>
	{$aLang.plugin.openstatcounter.about1}{$OpenstatcounterCode}{$aLang.plugin.openstatcounter.about2}
	<form method="post">
	    <fieldset>
		<label>{$aLang.plugin.openstatcounter.code} <input name="code" value="{$OpenstatcounterCode}"/></label>
	    </fieldset>
	    <fieldset>
	    	<input type="submit" value="{$aLang.plugin.openstatcounter.submit}">
	    </fieldset>
	</form>	
</div>

{include file='footer.tpl'}