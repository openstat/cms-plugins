<?php

// ==============================================================================================
// Licensed under the GPLv2 license
// ==============================================================================================
// @author     WEBO Software (http://www.webogroup.com/)
// @version    0.1
// @copyright  Copyright &copy; 2013 Openstat, All Rights Reserved
// ==============================================================================================
if (!defined ("TYPO3_MODE")) die ("Access denied.");

if(TYPO3_MODE=='FE') require_once(t3lib_extMgm::extPath('openstat_counter').'class.tx_openstatcounter.php');
#####################################################
## Hook for HTML-modification on the page   #########
#####################################################
// hook is called after Caching! => for modification of pages with COA_/USER_INT objects. 
$TYPO3_CONF_VARS['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-output'][] = 'EXT:openstat_counter/class.tx_openstatcounter_fehook.php:&tx_openstatcounter_fehook->intPages'; 
// hook is called before Caching! => for modification of pages on their way in the cache.
$TYPO3_CONF_VARS['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-all'][] = 'EXT:openstat_counter/class.tx_openstatcounter_fehook.php:&tx_openstatcounter_fehook->noIntPages'; 
#####################################################
?>