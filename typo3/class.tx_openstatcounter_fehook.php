<?php

// ==============================================================================================
// Licensed under the GPLv2 license
// ==============================================================================================
// @author     WEBO Software (http://www.webogroup.com/)
// @version    0.1
// @copyright  Copyright &copy; 2013 Openstat, All Rights Reserved
// ==============================================================================================
require_once (t3lib_extMgm::extPath ("openstat_counter")."class.tx_openstatcounter.php");

class tx_openstatcounter_fehook extends tslib_pibase {    
    function intPages (&$params,&$that) {}
    function noIntPages (&$params,&$that) {}
}

if (defined("TYPO3_MODE") && $TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/openstat_counter/class.tx_openstatcounter_fehook.php"]){
        include_once($TYPO3_CONF_VARS[TYPO3_MODE]["XCLASS"]["ext/openstat_counter/class.tx_openstatcounter_fehook.php"]);
}
$openstatcounter_settings = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['openstat_counter']);
global $openstatcounter_id;
$openstatcounter_id = $openstatcounter_settings['code'];
if (empty($openstatcounter_id)) {
    require_once(dirname(__FILE__) . '/openstat.counter.api.php');
    $email = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($GLOBALS['TYPO3_DB']->exec_SELECTquery('email', 'be_users', 'admin=1 AND disable=0', '', '', 1));
    $openstatcounter_id = openstat_counter_api_add($email['email'], '', 'WEBO@Typo3 ' . TYPO3_version);
    $openstatcounter_settings = @file_get_contents(dirname(dirname(dirname(__FILE__))) . '/LocalConfiguration.php');
    $openstatcounter_settings = preg_replace("!.*'openstat_counter'[^,]+,!i", "'openstat_counter' => '" . serialize(array('code' => $openstatcounter_id)) . "',", $openstatcounter_settings);
    if (!empty($openstatcounter_settings)) {
	@file_put_contents(dirname(dirname(dirname(__FILE__))) . '/LocalConfiguration.php', $openstatcounter_settings);
    }
}

ob_start('openstatcounter_code');

function openstatcounter_code ($content) {
    if (!empty($content)) {
	global $openstatcounter_id;
	require_once(dirname(__FILE__) . '/openstat.counter.api.php');
	return preg_replace("!(</body>)!is", openstat_counter_api_code($openstatcounter_id) . "$1", $content);
    }
}

?>
